<?php
/*
*	字符串处理函数，用于CI模板的方法
*	author kphcdr@163.com
*/
function len($str, $len=10, $etc='...')
{
		$restr = '';
		$i = 0;
		$n = 0.0;
	
		//字符串的字节数
		$strlen = strlen($str);
		while(($n < $len) and ($i < $strlen))
		{
		   $temp_str = substr($str, $i, 1);
	
		   //得到字符串中第$i位字符的ASCII码
		   $ascnum = ord($temp_str);
	
		   //如果ASCII位高与252
		   if($ascnum >= 252) 
		   {
				//根据UTF-8编码规范，将6个连续的字符计为单个字符
				$restr = $restr.substr($str, $i, 6); 
				//实际Byte计为6
				$i = $i + 6; 
				//字串长度计1
				$n++; 
		   }
		   elseif($ascnum >= 248)
		   {
				$restr = $restr.substr($str, $i, 5);
				$i = $i + 5;
				$n++;
		   }
		   elseif($ascnum >= 240)
		   {
				$restr = $restr.substr($str, $i, 4);
				$i = $i + 4;
				$n++;
		   }
		   elseif($ascnum >= 224)
		   {
				$restr = $restr.substr($str, $i, 3);
				$i = $i + 3 ;
				$n++;
		   }
		   elseif ($ascnum >= 192)
		   {
				$restr = $restr.substr($str, $i, 2);
				$i = $i + 2;
				$n++;
		   }
	
		   //如果是大写字母 I除外
		   elseif($ascnum>=65 and $ascnum<=90 and $ascnum!=73)
		   {
				$restr = $restr.substr($str, $i, 1);
				//实际的Byte数仍计1个
				$i = $i + 1; 
				//但考虑整体美观，大写字母计成一个高位字符
				$n++; 
		   }
	
		   //%,&,@,m,w 字符按1个字符宽
		   elseif(!(array_search($ascnum, array(37, 38, 64, 109 ,119)) === FALSE))
		   {
				$restr = $restr.substr($str, $i, 1);
				//实际的Byte数仍计1个
				$i = $i + 1;
				//但考虑整体美观，这些字条计成一个高位字符
				$n++; 
		   }
	
		   //其他情况下，包括小写字母和半角标点符号
		   else
		   {
				$restr = $restr.substr($str, $i, 1);
				//实际的Byte数计1个
				$i = $i + 1; 
				//其余的小写字母和半角标点等与半个高位字符宽
				$n = $n + 0.5; 
		   }
		}
	
		//超过长度时在尾处加上省略号
		if($i < $strlen)
		{
		   $restr = $restr.$etc;
		}
	
		return $restr;
}
function delhtml($str)
{
	$str = strip_tags($str);

	//首先去掉头尾空格
	$str = trim($str);

	//接着去掉两个空格以上的
	$str = preg_replace('/\s(?=\s)/', '', $str);

	//最后将非空格替换为一个空格
	$str = preg_replace('/[\n\r\t]/', ' ', $str);

	return $str;
}
function nolink()
{
	echo 'javascript:;';
}
function u($str='')
{
	echo site_url($str);
}
function b($str='')
{
	echo base_url($str);
}